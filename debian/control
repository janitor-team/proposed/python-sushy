Source: python-sushy
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
 Michal Arbet <michal.arbet@ultimum.io>,
Build-Depends:
 debhelper-compat (= 10),
 dh-python,
 openstack-pkg-tools,
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 python3-coverage,
 python3-dateutil,
 python3-hacking,
 python3-openstackdocstheme,
 python3-oslotest,
 python3-requests,
 python3-stestr,
 python3-stevedore,
 python3-sphinxcontrib.apidoc,
 subunit,
Standards-Version: 4.4.1
Vcs-Browser: https://salsa.debian.org/openstack-team/libs/python-sushy
Vcs-Git: https://salsa.debian.org/openstack-team/libs/python-sushy.git
Homepage: https://docs.openstack.org/sushy

Package: python-sushy-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: small library to communicate with Redfish based systems - doc
 Sushy is a Python library to communicate with Redfish based systems. The goal
 of the library is to be extremely simple, small, have as few dependencies as
 possible and be very conservative when dealing with BMCs by issuing just
 enough requests to it (BMCs are very flaky).
 .
 Therefore, the scope of the library has been limited to what is supported by
 the OpenStack Ironic project. As the project grows and more features from
 Redfish are needed Sushy will expand to fulfil those requirements.
 .
 This package contains the documentation.

Package: python3-sushy
Architecture: all
Depends:
 python3-pbr (>= 2.0.0),
 python3-requests (>= 2.14.2),
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-sushy-doc,
Description: small library to communicate with Redfish based systems - Python 3.x
 Sushy is a Python library to communicate with Redfish based systems. The goal
 of the library is to be extremely simple, small, have as few dependencies as
 possible and be very conservative when dealing with BMCs by issuing just
 enough requests to it (BMCs are very flaky).
 .
 Therefore, the scope of the library has been limited to what is supported by
 the OpenStack Ironic project. As the project grows and more features from
 Redfish are needed Sushy will expand to fulfil those requirements.
 .
 This package contains the Python 3.x module.
